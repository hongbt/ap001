package com.example.ap04;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import android.app.Activity;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    Button btnSave,btnXoa,btnExit;
    EditText Edt_TieuDe, Edt_NoiDung;
    ListView list_DanhSach;

    ArrayList<String> mylist;
    ArrayAdapter<String> myadapter;
    SQLiteDatabase mydatabase;
    private String selectedTitle;
    private String selectedContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Khởi tạo và kết nối với các view trong layout
        btnSave = findViewById(R.id.btnSave);
        btnXoa = findViewById(R.id.btnXoa);
        btnExit = findViewById(R.id.btnExit);
        Edt_TieuDe = findViewById(R.id.Edt_TieuDe);
        Edt_NoiDung = findViewById(R.id.Edt_NoiDung);
        list_DanhSach = findViewById(R.id.list_DanhSach);

        // Khởi tạo danh sách và adapter
        mylist = new ArrayList<>();
        myadapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, mylist);
        list_DanhSach.setAdapter(myadapter);

        // Tạo và mở cơ sở dữ liệu SQLite
        mydatabase = openOrCreateDatabase("List_Ghi_Chú.db", MODE_PRIVATE, null);

        // Tạo Table để chứa dữ liệu
        mydatabase.execSQL("CREATE TABLE IF NOT EXISTS tbGhiChu(TiêuĐề TEXT PRIMARY KEY, NộiDung TEXT)");

        // Xử lý sự kiện khi nhấn nút Lưu
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Lấy dữ liệu từ EditText
                String tieuDe = Edt_TieuDe.getText().toString();
                String noiDung = Edt_NoiDung.getText().toString();

                // Lưu vào cơ sở dữ liệu
                ContentValues values = new ContentValues();
                values.put("TiêuĐề", tieuDe);
                values.put("NộiDung", noiDung);
                mydatabase.insert("tbGhiChu", null, values);



                // Xóa thông tin hiện tại từ EditText
                Edt_TieuDe.setText("");
                Edt_NoiDung.setText("");

                // Cập nhật danh sách ghi chú sau khi thêm ghi chú
                updateNoteList();
            }
        });

        // Xử lý khi chọn một phần tử từ danh sách
        list_DanhSach.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @SuppressLint("Range")
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Lấy dữ liệu từ cơ sở dữ liệu dựa trên vị trí của phần tử được chọn
                selectedTitle = mylist.get(position);

                // Lấy nội dung của ghi chú được chọn từ cơ sở dữ liệu
                Cursor cursor = mydatabase.rawQuery("SELECT * FROM tbGhiChu WHERE TiêuĐề = ?", new String[]{selectedTitle});

                if (cursor.moveToFirst()) {
                    selectedContent = cursor.getString(cursor.getColumnIndex("NộiDung"));
                    // Hiển thị dữ liệu trong EditText để cho phép sửa đổi
                    Edt_TieuDe.setText(selectedTitle);
                    Edt_NoiDung.setText(selectedContent);
                }

                // Đóng con trỏ
                cursor.close();
            }
        });


        // Xử lý sự kiện khi nhấn nút xóa
        btnXoa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Kiểm tra xem tiêu đề đã được chọn hay chưa
                if (selectedTitle != null) {
                    // Xóa ghi chú từ cơ sở dữ liệu dựa trên tiêu đề đã chọn
                    int result = mydatabase.delete("tbGhiChu", "TiêuĐề = ?", new String[]{selectedTitle});
                    // Cập nhật danh sách ghi chú
                    Log.d("TAG", "delete: "+result);
                    updateNoteList();
                    // Thông báo xóa thành công
                    Toast.makeText(MainActivity.this, "Đã xóa ghi chú", Toast.LENGTH_SHORT).show();
                } else {
                    // Nếu không có ghi chú nào được chọn, hiển thị thông báo
                    Toast.makeText(MainActivity.this, "Vui lòng chọn ghi chú để xóa", Toast.LENGTH_SHORT).show();
                }
            }
        });

        //xử lý sự kiện khi nhấn nút thoát
        btnExit.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishAffinity();
            }
        } );

        // Xử lý khi chọn một phần tử từ danh sách
        list_DanhSach.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Lấy dữ liệu từ cơ sở dữ liệu dựa trên vị trí của phần tử được chọn
                selectedTitle = mylist.get(position);
                Cursor cursor = mydatabase.rawQuery("SELECT * FROM tbGhiChu WHERE TiêuĐề = ?", new String[]{selectedTitle});
                if (cursor.moveToFirst()) {
                    @SuppressLint("Range") String noiDung = cursor.getString(cursor.getColumnIndex("NộiDung"));
                    // Hiển thị dữ liệu trong EditText để cho phép sửa đổi
                    Edt_TieuDe.setText(selectedTitle);
                    Edt_NoiDung.setText(noiDung);
                }
                cursor.close();
            }
        });

        // Xử lý khi chọn xóa một phần tử từ danh sách
        list_DanhSach.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                // Lấy tiêu đề của phần tử được chọn
                String selectedTitle = mylist.get(position);
                // Xóa phần tử từ cơ sở dữ liệu
                mydatabase.delete("tbGhiChu", "TiêuĐề = ?", new String[]{selectedTitle});
                // Cập nhật danh sách ghi chú
                updateNoteList();
                return true;
            }
        });

        ViewCompat.setOnApplyWindowInsetsListener( findViewById( R.id.main ), (v, insets) -> {
            Insets systemBars = insets.getInsets( WindowInsetsCompat.Type.systemBars() );
            v.setPadding( systemBars.left, systemBars.top, systemBars.right, systemBars.bottom );
            return insets;
        } );

        // Hiển thị dữ liệu từ cơ sở dữ liệu lên danh sách
        updateNoteList();
    }

    // Phương thức để cập nhật danh sách ghi chú từ cơ sở dữ liệu
    private void updateNoteList() {
        mylist.clear();
        Cursor cursor = mydatabase.rawQuery("SELECT * FROM tbGhiChu", null);
        if (cursor.moveToFirst()) {
            do {
                @SuppressLint("Range") String tieuDe = cursor.getString(cursor.getColumnIndex("TiêuĐề"));
                mylist.add(tieuDe);
            } while (cursor.moveToNext());
        }
        cursor.close();
        myadapter.notifyDataSetChanged();
    }


}